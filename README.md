# 📝 README

In this project, we are keeping track of our efforts to make more code contributions and to make it easier for other Support Engineers to make code contributions. 

  - ℹ️  Code contributions deflect tickets! 


## 📓  History

In June 2023, we [completed](https://gitlab.com/manuelgrabowski/team-tracking/-/issues/27) `v0` of improving the [team tracking](https://gitlab.com/manuelgrabowski/team-tracking) project. We worked well together and wanted to continue coding together. 


### Code Contribution Modules

  - [ ] [Code Contributions - Brie Carranza](https://gitlab.com/gitlab-com/support/support-training/-/issues/3295)
  - [ ] [Manuel Grabowski – Code Contributions](https://gitlab.com/gitlab-com/support/support-training/-/issues/3039)
 
